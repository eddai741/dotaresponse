/*
info @ https://github.com/JakeLove/DiscordDotaRepsonseBot
*/

const fs = require("fs")
const Discord = require("discord.js")
const opus = require("node-opus")
const queue = require("queue")

const heroes = ["Zeus", "Witch_Doctor", "Windranger", "Vengeful_Spirit", "Tidehunter", "Sven", "Storm_Spirit", "Shadow_Shaman", "Sand_King", "Razor", "Pudge", "Puck", "Necrophos", "Morphling", "Mirana", "Lion", "Lina", "Lich", "Earthshaker", "Drow_Ranger", "Dazzle", "Crystal_Maiden", "Axe", "Anti-Mage", "Slardar", "Enigma", "Viper", "Tiny", "Faceless_Void", "Venomancer", "Nature's_Prophet", "Clockwerk", "Sniper", "Dark_Seer", "Pugna", "Beastmaster", "Shadow_Fiend", "Leshrac", "Enchantress", "Tinker", "Weaver", "Night_Stalker", "Spectre", "Ancient_Apparition", "Doom", "Chen", "Juggernaut", "Kunkka", "Bloodseeker", "Riki", "Wraith_King", "Queen_of_Pain", "Broodmother", "Jakiro", "Huskar", "Batrider", "Omniknight", "Dragon_Knight", "Warlock", "Alchemist", "Lifestealer", "Death_Prophet", "Ursa", "Bounty_Hunter", "Spirit_Breaker", "Silencer", "Invoker", "Clinkz", "Outworld_Devourer", "Bane", "Shadow_Demon", "Lycan", "Lone_Druid", "Brewmaster", "Phantom_Lancer", "Treant_Protector", "Ogre_Magi", "Phantom_Assassin", "Gyrocopter", "Chaos_Knight", "Rubick", "Luna", "Undying", "Disruptor", "Templar_Assassin", "Naga_Siren", "Visage", "Nyx_Assassin", "Keeper_of_the_Light", "Meepo", "Magnus", "Centaur_Warrunner", "Slark", "Timbersaw", "Medusa", "Troll_Warlord", "Tusk", "Bristleback", "Skywrath_Mage", "Elder_Titan", "Abaddon", "Ember_Spirit", "Earth_Spirit", "Legion_Commander", "Terrorblade", "Phoenix", "Techies", "Oracle", "Winter_Wyvern", "Arc_Warden", "Underlord", "Monkey_King", "Pangolier", "Dark_Willow"]

const loginToken = JSON.parse(fs.readFileSync("loginToken.json", "utf8")).token
//object containing preset messages for the bot to post
const botMessages = JSON.parse(fs.readFileSync("messages.json", "utf8"))

//function to set up bot event handlers
function startBot() {
	const bot = new Discord.Client()
	bot.login(loginToken)

	bot.on("ready", () => {
		console.log("Hello there!")
		bot.user.setPresence({
			game: {
				name: "use $info",
				url: "https://github.com/JakeLove/DiscordDotaRepsonseBot"
			}
		})

		//Look at all connected guilds and give each guild a Q. Q contains an async queue of promises to play detected responses as mp3s
		guildIds = bot.guilds.keyArray()
		for (var i = guildIds.length - 1; i >= 0; i--) {
			const guild = bot.guilds.get(guildIds[i])
			guild.Q = new queue({
				concurrency: 1,
				autostart: true
			})
			//make sure to leave the channel when the queue as finished
			guild.Q.on("end", () => {
				guild.voiceConnection.channel.leave()
			})
			console.log("Connected to: " + guild.name)
		}
		console.log(bot.guilds.size + " total guilds!")
	})

	//give newly joined guilds a Q
	bot.on("guildCreate", (guild) => {
		console.log("Joined new guild: " + guild.name + "\nNow connected to " + bot.guilds.size + " guilds!")
		guild.Q = new queue({
			concurrency: 1,
			autostart: true
		})
		guild.Q.on("end", () => {
			guild.voiceConnection.channel.leave()
		})
	})

	bot.on("message", (message) => {
		if (message.content == "$info") {
			//post basic bot infomation from the messages.json
			message.channel.send(botMessages.info)
		}
		if (message.content == "$stop") {
			message.guild.Q.jobs = []
		} else {
			member = message.member
			if (member !== null) {
				const voiceChannel = member.voiceChannel
				const guild = message.guild

				//check if user is in a channel before trying to find a response
				if (voiceChannel !== undefined) {
					//do not look for a response if the queue is full (5 is the limit)
					if (guild.Q.jobs.length >= 5) {
						//post message alerting user queue is full
						console.log("Max Q reached in guild: ", guild.name)
						message.channel.send(botMessages.qLimit);
					} else if (message.content.split(" ")[0] == "random") {
						const hero = message.content.split("-")[1]
						if (typeof hero !== 'undefined') {
							const heroResponses = responses.filter(obj => obj.hero == hero)
							const heroMp3List = heroResponses.map(res => res.url.split("/").pop())
							const mp3path = "./responseData/mp3s/" + heroMp3List[Math.floor(Math.random() * heroMp3List.length)]
							console.log("Guild", guild.name, "is playing:", mp3path)
							guild.Q.push(function() {
								return playMp3(mp3path, voiceChannel)
							})
						} else {
							//pick a random mp3 file
							const randomMp3 = mp3List[Math.floor(Math.random() * mp3List.length)]
							const mp3path = "./responseData/mp3s/" + randomMp3
							//add it to the queue, it will autoplay thanks to the queue modules "queue.autostart" feature
							console.log("Guild", guild.name, "is playing:", randomMp3)
							message.channel.send("You just heard: ||" + randomMp3.split(".")[0] + "||")
							guild.Q.push(function() {
								return playMp3(mp3path, voiceChannel)
							})
						}
					} else {
						//split the message up be square brackets
						const msgSplit = message.content.split("-")
						//if there are 2 or more parts the message contains square brackets so a hero needs to be specified
						const hero = msgSplit.length >= 2 ? msgSplit[1] : undefined
						//the response to search will be before any square brackets, so at index 0
						console.log(msgSplit)
						const quote = msgSplit[0].trim()

						//get the filename of the closest matching response
						const responseFileName = searchForResponse(quote, hero)

						if (responseFileName) {
							const mp3path = "./responseData/mp3s/" + responseFileName
							console.log("Guild", guild.name, "is playing:", responseFileName)
							guild.Q.push(function() {
								return playMp3(mp3path, voiceChannel)
							})
						}
					}
				} else {
					//alert user they need to be in a voice channel to use the $random command
					if (message.content.split(" ")[0] == "random") {
						message.channel.send(botMessages.noVoiceChannel)
					}
				}
			}

		}
	})
}

//worlds most advanced error handler
function handlerErr(err) {
	console.log("error: ", err)
	console.error(err)
}

//creates a promise to play and mp3 found at <filepath> in the channel <voiceChannel>
function playMp3(filepath, voiceChannel) {
	return new Promise((resolve, reject) => {
		voiceChannel.join()
			.then(connection => {
				const dispatcher = connection.playFile(filepath)

				dispatcher.on("end", () => {
					resolve()
				})

				dispatcher.on("error", (err) => {
					handleErr(err)
					reject()
				})
			}).catch((err) => {
				handlerErr(err)
				reject()
			})
	})
}

//searches the responses object for quotes matching the query. Returns file name of the quotes mp3
function searchForResponse(query, hero) {
	//remove special chars and case so the user does not need to worry about them
	query = query.replace(/[^a-zA-Z ]/g, "").toLowerCase()

	if (hero === undefined) {
		responseSample = responses
	} else {
		//filter out heros that do not match the one specified
		responseSample = responses.filter(res => res.hero == hero)
	}

	//calculate a similarity score of all responses in the sample compared to the query, then put them in an array
	scores = responseSample.map(res => stringSimilarity(res.quote, query))

	//list of the indexs of the best matches
	bestMatches = []
	//similarity score of the best matches
	bestMatchScore = 0

	//create a list of all matches with the highest score
	for (var i = scores.length - 1; i >= 0; i--) {
		//add quotes of equal similarity to best matches
		if (scores[i] == bestMatchScore) {
			bestMatches.push(i)
		}

		//once a quote with a better score is found update the bestMatchScore and reset the list of matches
		if (scores[i] > bestMatchScore) {
			bestMatchScore = scores[i]
			bestMatches = [i]
		}
	}

	//only return a filepath if the best score is above 0.9
	if (bestMatchScore > 0.9) {
		//pick a random element from best matches from the
		randomIndex = bestMatches[Math.floor(Math.random() * bestMatches.length)]
		//get the url of chosen quote
		quoteUrl = responseSample[randomIndex].url
		//return file name from the url
		return quoteUrl.split("/").pop()
	} else {
		return false
	}
}

//returns a score between 0 and 1 determining the similarity between the 2 strings a and b.
//looks at character and bigrams only so: "yes yes yes" matches very well to "es yes yes y"
//NEEDS WORK???
function stringSimilarity(a, b) {
	let score = 0
	let matches = 0
	let maxMatches = 0

	for (let n = 2; n > 0; n--) {
		A = ngrams(a, n)
		B = ngrams(b, n)

		maxMatches = A.length > B.length ? A.length : B.length
		matches = 0
		for (let i = A.length - 1; i >= 0; i--) {
			if (B.indexOf(A[i]) >= 0) {
				matches++
			}
		}

		score = score + 0.5 * (matches / maxMatches)
	}

	return score
}

//returns a list of all n grams of length n in string a
function ngrams(a, n) {
	let ngrams = []

	for (var i = a.length - n; i >= 0; i--) {
		ngrams.push(a.substring(i, i + n))
	}

	return ngrams
}


//list to store all the response objects in. a response object looks like: {url:<url>, quote:<quote>, hero:<hero>}
let responses = []
//store all mp3s filenames in their own seperate list for randomSampling. A bit memory inefficient but it works well enough.
let mp3List = []

//load response data into the list <responses>
for (let h = heroes.length - 1; h >= 0; h--) {
	hero = heroes[h]
	heroData = JSON.parse(fs.readFileSync("./responseData/library/" + hero + ".json", "utf8"))

	urls = heroData.URLs
	//remove special chars and case so the user does not need to worry about them
	quotes = (heroData.text).map(x => x.replace(/[^a-zA-Z ]/g, "").toLowerCase())

	for (var i = urls.length - 1; i >= 0; i--) {
		quote = quotes[i]
		url = urls[i]
		hero = hero.replace(/[^a-zA-Z ]/g, "").toLowerCase()
		responses.push({
			url: url,
			quote: quote,
			hero: hero
		})
	}
}

mp3List = responses.map(res => res.url.split("/").pop())

startBot()
