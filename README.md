# Discord Dota 2 Response Bot

## Fork 

This is a fork of the 
[Dota 2 Response Bot](https://github.com/JakeLove/DiscordDotaRepsonseBot) 
for Discord by Jake Love. 

The original bot no longer works (does not play responses), 
so I have fixed the issues.

For the original README and installation guide check 
[here](https://github.com/JakeLove/DiscordDotaRepsonseBot)

Type `$info` in discord for the command list.

## Additions

*  `random -hero` plays a random line from specified hero

*  `random` plays a random line from any hero (same as original bot), 
    but the Bot will now reply with the ID in spoiler tags.


