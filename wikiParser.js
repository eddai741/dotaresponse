const https = require('https')
const fs = require('fs')

const heroes = ["Zeus", "Witch_Doctor", "Windranger", "Vengeful_Spirit", "Tidehunter", "Sven", "Storm_Spirit", "Shadow_Shaman", "Sand_King", "Razor", "Pudge", "Puck", "Necrophos", "Morphling", "Mirana", "Lion", "Lina", "Lich", "Earthshaker", "Drow_Ranger", "Dazzle", "Crystal_Maiden", "Axe", "Anti-Mage", "Slardar", "Enigma", "Viper", "Tiny", "Faceless_Void", "Venomancer", "Nature's_Prophet", "Clockwerk", "Sniper", "Dark_Seer", "Pugna", "Beastmaster", "Shadow_Fiend", "Leshrac", "Enchantress", "Tinker", "Weaver", "Night_Stalker", "Spectre", "Ancient_Apparition", "Doom", "Chen", "Juggernaut", "Kunkka", "Bloodseeker", "Riki", "Wraith_King", "Queen_of_Pain", "Broodmother", "Jakiro", "Huskar", "Batrider", "Omniknight", "Dragon_Knight", "Warlock", "Alchemist", "Lifestealer", "Death_Prophet", "Ursa", "Bounty_Hunter", "Spirit_Breaker", "Silencer", "Invoker", "Clinkz", "Outworld_Devourer", "Bane", "Shadow_Demon", "Lycan", "Lone_Druid", "Brewmaster", "Phantom_Lancer", "Treant_Protector", "Ogre_Magi", "Phantom_Assassin", "Gyrocopter", "Chaos_Knight", "Rubick", "Luna", "Undying", "Disruptor", "Templar_Assassin", "Naga_Siren", "Visage", "Nyx_Assassin", "Keeper_of_the_Light", "Meepo", "Magnus", "Centaur_Warrunner", "Slark", "Timbersaw", "Medusa", "Troll_Warlord", "Tusk", "Bristleback", "Skywrath_Mage", "Elder_Titan", "Abaddon", "Ember_Spirit", "Earth_Spirit", "Legion_Commander", "Terrorblade", "Phoenix", "Techies", "Oracle", "Winter_Wyvern", "Arc_Warden", "Underlord", "Monkey_King", "Pangolier", "Dark_Willow"]
responseLibrary = {}

function createLibrary(i = 0){
	getResponses(heroes[i]).then((res) => {
		if(i >= heroes.length) {
			return
		} else {
			if(res == "error") {
				console.log("ERROR with: " + heroes[i] + ". Retrying")
				createLibrary(i)
			} else {
				fs.writeFile("responseData/library/" + heroes[i] + ".json", JSON.stringify(res), function(err) {
				    if(err) {
				        return console.log(err)
				    }

				    console.log("Done: " + heroes[i])
				    createLibrary(i + 1)
				})
			}
		}
	});
}

function getResponses(heroName){
	return new Promise((resolve, reject) => {
		const options = {
			hostname: 'dota2.gamepedia.com',
			port: 443,
			path: '/' + heroName +'/Responses',
			method: 'GET'
		}

		const req = https.request(options, (res) => {
			body = ""
			res.on('data', (chunk) => {
			  body = body + chunk
			})

			res.on('end', (d) => {
				try {
					responses = body.split('\n').filter(x => x.includes('.mp3'))

					responseUrls = []
					responseTexts = []
					for (let i = responses.length - 1; i >= 0; i--) {
						r = responses[i]

						//remove front <ul> tag if found
						if(r.includes("<ul>")) {
							r = r.substring(4)
						}
						//remove front <li> tag and href stuff
						r = r.substring(14)
						//url goes up to end of string
						URL = r.split("\"")[0]
						if(r.includes("</ul>")) {
							r = r.substring(0, r.length - 5)
						}
						//seperate by tags
						r = r.split(/[<>]/)
						if(r[r.length - 3] === undefined) {
							console.log(r)
						}
						text = r[r.length - 3].slice(1, -1)
						if(text != "") {
							responseUrls.push("h" + URL)
							responseTexts.push(text)
						}
					}

					resolve({URLs: responseUrls, text: responseTexts})
				}

				catch(err) {
					resolve("error")
				}
			})
		})

		req.on('error', (err) => {
			console.error(err)
		});

		req.end()
	})
}

createLibrary()
